#!/bin/sh
script_relative_dir=../../../scripts/
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "./")

if_mac
mkdir -p ~/Library/Fonts
cp -f $file_path/ttf/JetBrainsMono-Italic.ttf ~/Library/Fonts/
cp -f $file_path/ttf/JetBrainsMono-Bold.ttf ~/Library/Fonts/
cp -f $file_path/ttf/JetBrainsMono-BoldItalic.ttf ~/Library/Fonts/
cp -f $file_path/ttf/JetBrainsMono-Medium.ttf ~/Library/Fonts/
cp -f $file_path/ttf/JetBrainsMono-MediumItalic.ttf ~/Library/Fonts/
fi

if_linux
mkdir -p ~/.local/share/fonts/ttf/JetBrainsMono
cp -f $file_path/ttf/JetBrainsMono-Italic.ttf ~/.local/share/fonts/ttf/JetBrainsMono/
cp -f $file_path/ttf/JetBrainsMono-Bold.ttf ~/.local/share/fonts/ttf/JetBrainsMono/
cp -f $file_path/ttf/JetBrainsMono-BoldItalic.ttf ~/.local/share/fonts/ttf/JetBrainsMono/
cp -f $file_path/ttf/JetBrainsMono-Medium.ttf ~/.local/share/fonts/ttf/JetBrainsMono/
cp -f $file_path/ttf/JetBrainsMono-MediumItalic.ttf ~/.local/share/fonts/ttf/JetBrainsMono/
fc-cache
fi
