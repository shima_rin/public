#!/bin/sh
script_relative_dir=../../scripts/
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

software_list=$(sga -m0 -c "^software" $config_path \
	       | sga -m1 -b "software" | tr '\n' ',')

IFS=',' read -r -a software_array <<< "$software_list"

for i in "${software_array[@]}"
do
    if   [ -d "$file_dir/$i" ];then
	echo "---[$i] will be installed by dir---"
	echo sh $file_dir/$i/install.sh
    else
	cmd=$(sga -m 0 -c "install_$i" $file_dir/misc/install.sh)
	if [ -z "$cmd" ]; then
	    echo "---[$i] will be installed by simple install---"
	    echo install_simple $i || "do not know how to install this app"
	else
	    echo "---[$i] will be installed by misc function---"
	    echo eval install_$i
	fi
    fi
done
