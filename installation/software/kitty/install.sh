#!/bin/sh
script_relative_dir=../../../scripts/
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

echo "install kitty"
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin
mkdir -p ~/.config/kitty/
echo "include $file_dir/$machine.conf" >> ~/.config/kitty/kitty.conf

mkdir -p ~/Applications/

if_mac
echo "install MyKitty"
osacompile -o ~/Applications/MyKitty.app $file_dir/kitty.scpt
cp -f $file_dir/icons.icns ~/Applications/MyKitty.app/Contents/Resources/applet.icns
echo "need privacy support for kitty"
echo "open privatcy, assitantive, +, add this app to the list"
echo "cmd+ spc, search MyKitty, drag to the dock"
echo "sh $file_dir/update.sh to update kitty from anywhere"
pause_for_key
fi


echo "alias icat='kitty +kitten icat" >> ~/.zshrc
echo "alias icat='kitty +kitten icat" >> ~/.bashrc
echo "kitty & MyKitty installed..."
