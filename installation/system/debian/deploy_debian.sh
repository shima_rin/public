read -p "user name: " name
read -p "hostname: " hostname

sed -i '/DVD/d' /etc/apt/sources.list
sed -i '/CD/d' /etc/apt/sources.list
apt install -y curl
pubip=`curl checkip.amazonaws.com`

apt update
apt dist-upgrade
apt install -y zsh
echo "choose zh_CN.utf8 [468]"

dpkg-reconfigure locales
locale-gen

export LANG=zh_CN.utf8
export LANGUAGE=zh_CN.utf8
export LC_ALL=zh_CN.utf8

echo "export LANG=zh_CN.utf8" >> /root/.bashrc
echo "export LANGUAGE=zh_CN.utf8" >> /root/.bashrc
echo "export LC_ALL=zh_CN.utf8" >> /root/.bashrc

useradd -m -u 1000 -s /usr/bin/zsh $name || echo "already exists"
passwd $name
su -c "mkdir -p -m 700 ~/.ssh" $name

printf  "run gen_push_key.sh in your ~/.ssh directory\n"
printf  "use (profile_name) you like, ip $pubip and login $name. \n"
printf  "if ready, answer y...\n"
read yn
case $yn in
    [Yy]* ) break;;
    * ) echo "now exit..." && exit;;
esac

printf "if success, run ssh (profile_name) then type 2 to zsh promote.\n"
printf "if success type y...\n"
read yn 
case $yn in
    [Yy]* ) break;;
    * ) echo "now exit..." && exit;;
esac


sed -i -e '1iProtocol 2\' /etc/ssh/sshd_config
sed -i '/#PasswordAuthentication/c\PasswordAuthentication no' /etc/ssh/sshd_config
sed -i '/PasswordAuthentication/c\PasswordAuthentication no' /etc/ssh/sshd_config
sed -i '/#PermitEmptyPasswords/c\PermitEmptyPasswords no' /etc/ssh/sshd_config
sed -i '/X11Forwarding/c\X11Forwarding no' /etc/ssh/sshd_config
sed -i '/PermitRootLogin yes/c\PermitRootLogin no' /etc/ssh/sshd_config

systemctl restart sshd

apt install -y sudo gcc make
apt install -y git
apt install -y emacs25
apt install -y w3m w3m-el
apt install -y mosh
apt install -y tmux
apt install -y wget

mosh-server -p 60001

hostname $hostname
echo "$hostname" > /etc/hostname
echo "127.0.0.1 $hostname" >> /etc/hosts
echo "$name ALL=(ALL:ALL) ALL" >> /etc/sudoers

echo "finished...."
echo "add udp 60001 to firewall group for mosh"


echo "su $name"
echo "curl -fsSL https://gitlab.com/shima_rin/public/-/raw/main/deploy_debian/deploy_debian_sub.sh | sh"

