#!/bin/sh
script_relative_dir=../../scripts/
source $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}
# (bashInit "../")

echo "now restore various necssary files"

public_dir=$(dirname $(realpath $file_dir/../..))
echo $public_dir
cp -f .emacs ~/.emacs
cp -f .dict  ~/.dict
sga -m 100 -c "(setq public_dir " -r "(setq public_dir \"$public_dir\") ;;edited by restore.sh" ~/.emacs

config_get fav_song_playlist
config_get porn_playlist
config_get porn1_playlist

cp -f ./fav_song_playlist $fav_song_playlist
mkdir -p $porn_playlist
mkdir -p $porn1_playlist

config_get neofetch_picture
mkdir -p $(dirname $neofetch_picture)
cp -f ./neofetch_picture $neofetch_picture

cp -rf .ssh ~/.ssh
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*
cp $script_dir/gen_push_key.sh ~/.ssh
rm -f ~/.ssh/known_hosts
