;; (for '(i j k) 0 '(3 2 4)  "db_ptr->" "mediate." '(price vol) t)
;; (for '(i j k) '(0 1 2) '(3 2 4)  "db_ptr->" "" '(price vol))

;; (genSwitch 'suffix)
;; (genSwitch 'suffix '(ryze ayaya "'chino'"))
;; (genEnum 'krr  '(ryze chino ayaya))

;; (enum2str)

;; (genStruct "DataBox")
;; (printStruct "data_box")
;; (printStruct "data_box" ".2")

;;(myReplace "[*]" "<*>" "double" "" "number" "(1+ *)")
;;(myReplace "[*]" "_<*>_" "double" "" "number")
;;(myReplace "::" "->" "double" "BidVolume5")
;;(myReplace "\"*\"" "*" "double" "BidVolume1")
;;(myReplace "double" "int" "*BEGIN*")

;; showcase
;; (genHeader t "databox")
;; (genOO "cell" "stats")

;; (genSwitch 'person '(ryze ayaya chino))
;; (genEnum 'cell_type '(big small) t)
;; (genEnum 'cell_type '(big small))
;; (enum2str)




(defun genDefineLCase(lst)
  (next-line 2)
  (mapcar (lambda (arg)
	    (setq str (format "%s" arg))
	    (insert (format "#define h3_%s H3_%s\n" str (upcase str)))
	    ) lst)
  )

(defun replaceBadChar(beg end)
  "strip and replace bad char"
  (progn
    (myReplace "É" "E" beg end)
    (myReplace "Å" "A" beg end)
    (myReplace "Ô" "O" beg end)
    (myReplace "Í" "I" beg end)
    (myReplace "Ã" "A" beg end)
    (myReplace "Ç" "C" beg end)
    ))

(defun replaceDefIfExist (beg end)
  (setf save (point))
  (next-line)
  (setq str (line-str))
  (if (string-match beg str)
      (progn
	(goto-char (line-beginning-position))
	(kill-line)
	(kill-line)
	(setq if_break t)
	(while if_break
	  (let* ((str (line-str)))
	    (setf if_break (not (string-match end str)))
	    (kill-line)
	    )
	  )
	)
    )
  (kill-line)
  (goto-char save)
  )


(defun string-has-no-lowercase (string)
  "Return true iff STRING has no lowercase"
  (equal (upcase string) string))


(defmacro assert (test-form)
  `(when (not ,test-form)
     (error "Assertion failed: %s" (format "%s" ',test-form))))

(defun line-str()
  (buffer-substring-no-properties
   (line-beginning-position)
   (line-end-position))
  )

(format  "%s" "'oeu'")

;; examples
;; find "NONE" and "CTP:**"
(defun parseXml()
  (let ((counter -1)
	)
    (while (progn 
	     (setq counter (+ counter 1))
	     (forward-line 1)
	     (not (looking-at "^$")))
      (let* ((str (line-str))
	     (num1 (+ (string-match "\"" str) 1))
	     (num2 (string-match "\"" str  num1 ))
	     (str1   (substring str num1 num2))
	     (num1 (+ (string-match "t=\"" str) 3))
	     (num2 (string-match "\"" str  num1 ))
	     (str2   (substring str num1 num2))
	     )
	(insert (format "case %d: sprintf(msg,\"%s %s\"); break;" counter  str1 str2))
	(kill-line))
      )
    )
  )

;;(parseXml)
;;<error id="NONE" value="0" prompt="CTP:正确"/>
;;<error id="INVALID_DATA_SYNC_STATUS" value="1" prompt="CTP:不在已同步状态"/>
;;<error id="INCONSISTENT_INFORMATION" value="2" prompt="CTP:会话信息不一致"/>

(defun getSwitchInfo ()
  (let ((begin (point))
	(case_name nil)
	(case_str  nil)
	(line nil)
	(tmp_str nil)
	(line-count 0)
	)
    (forward-line 1)
    (if (looking-at "switch")
	(progn
	  (incf line-count)
	  (while
	      (progn
		(forward-line 1)
		(incf line-count)
		(setq line (line-str))
		(not (looking-at "default"))
		)
	    (progn
	      (if (looking-at "case")
		  (progn
		    (setq case_name
			  (append case_name
				  (list line)))
		    (setq case_str
			  (append case_str
				  (list tmp_str)))
		    (setq tmp_str line)
		    )
		(setq tmp_str (format "%s\n%s"
				      tmp_str
				      line))
		)
	      )
	    )
	  (setq case_name
		(append case_name (list "default:")))
	  (setq case_str
		(append case_str
			(list tmp_str)))
	  (setq tmp_str line)
	  (while
	      (progn
		(forward-line 1)
		(incf line-count)
		(setq line (line-str))
		(not (looking-at "\tbreak;"))
		)
	    (setq tmp_str (format "%s\n%s"
				  tmp_str
				  line))
	    )
	  (setq tmp_str (format "%s\n%s"
				tmp_str
				line))
	  (setq case_str
		(append case_str
			(list tmp_str)))
	  (incf line-count)
	  )
      )
    (goto-char begin)
    (list (mapcar* #'cons case_name (cdr case_str))
	  line-count)
    )
  )


(defun genSwitch (name &optional arg)
  (let* ((region_begin (point))
	 (info (getSwitchInfo))
	 (line-count (cadr info))
	 (info (car info))
	 (find nil)
	)
    (if (eq (car info) nil)
	(progn
	  (insert (format "\nswitch (%s) {\n" name))
	  (mapcar (lambda (ele)
		    (progn
		      (insert (format "case %s:\nbreak;\n" ele)))) arg) 
	  (insert "default:\nbreak;\n}\n")
	  )
      (progn
	(insert (format "\nswitch (%s) {\n" name))
	(mapcar
	 (lambda (ele)
	   (progn
	     (setq find nil)
	     (mapcar
	      (lambda (part)
		(if (string-match
		     (car part)
		     (format "case %s:" ele))
		    (progn
		      (setq find t)
		      (insert (format "%s\n" (cdr part)))
		      )
		  )) info)
	     (if (eq find nil)
		 (insert (format "case %s:\nbreak;\n" ele))
	       )
	     )
	   ) arg)
	(mapcar
	 (lambda (part)
	   (progn
	     (setq find nil)
	     (mapcar
	      (lambda (ele)
		(if (string-match
		     (car part)
		     (format "case %s:" ele))
		    (setq find t))) arg)
	     (if (eq find nil)
		 (insert (format "%s\n" (cdr part))))
	       )) info)
	(insert "}\n")
	)
      )
    (setq region_end (point))
    (indent-region region_begin region_end)
    (forward-line 1)
    (kill-line line-count)
    (goto-char region_begin)
    info
    )
  )



(defun genEnum (name lst &optional arg)
  (let ((header (upcase (format "%s" name)))
	(index 0)
	(begin (point))
	)
    (replaceDefIfExist (format "enum %s_enum {" name) "};")
    (insert (format "\nenum %s_enum {\n" name))
    (if (eq arg nil)
	(insert (format " %s_NULL = %d,\n" header -1)))
    (mapcar #'(lambda (item)
		(if (eq arg nil)
		    (progn
		      (insert (format "%s_%s = %d,\n"
				      header
				      (upcase (format "%s" item))
				      index))
		      (setf index (1+ index))
		      )
		  (progn
		    (insert (format "%s_%s = 0b1%s,\n"
				    header
				    (upcase (format "%s" item))
				    (make-string index ?0)))
		    (setf index (1+ index))
		    )
		  ))
	    lst)
    (if (eq arg nil)
	(insert (format "%s_NUM\n};" header))
      (insert (format "};" header)))
    (indent-region begin (point))
    (goto-char begin)
    )
  )

;; (genEnum 'krr  '(chino rin ayaya))     ;normal rep
;; (genEnum 'krr  '(chino rin ayaya) t)   ;binary rep


(defun enum2str (&optional arg)
  (let* ((index (if (eq arg nil) -1 arg))
	 (str "")
	 (str1 "")
	 (output "")
	 (num1 nil)
	 (num2 nil)
	 )
    (progn
      (forward-line 1)
      (setq str (line-str))
      (setq num1 (+ (string-match "enum" str) 4))
      (setq num2 (string-match "{" str  num1 ))
      (setq str1 (format "%s" (string-trim (substring str num1 num2))))

      (setq output (concat output (format "void %s2str(char *str, %s item) {\n" str1 str1)))
      (setq output (concat output (format "switch (item) {\n")))

      (while
	  (progn
	    (forward-line 1)
	    (not (looking-at "}")))
	(progn
	  (setq str (line-str))
	  (setq num1 (string-match "[,=]" str))
	  (setq str1 (if (eq num1 nil)
			 (string-trim str)
		       (format "%s" (string-trim (substring str 0 num1)))))
	  (setq output (concat output (format "case %d: sprintf(str,  \"%s\"); break;\n" index  str1)))
	  (setq index (+ index 1))
	  )
	)
      (forward-line 1)
      (setq output (concat output (format "}\n }\n")))
      (insert output)
      )
    )
  )

;;(enum2str)
(defun ignoreWarn (x)
  (setq beg (point))
  (newline)
  (insert (format "H3_DISABLE_COMPILER_WARNING (%s);\n"
		  x))
  (insert "#define \nH3_ENABLE_COMPILER_WARNING();\n")
  (goto-char beg)
  )

(defun genHeader (&optional cpp prefix)
  ;; naming convention should be DataBox db;
  ;; function should be data_box_xxx()
  (setq name (file-name-base (buffer-file-name)))
    (newline)
    (insert (format "#ifndef %s_%s_H" (upcase prefix) (upcase name)))
    (newline)
    (insert (format "#define %s_%s_H" (upcase prefix) (upcase name)))
    (newline)
    (if cpp
	(progn
	  (insert "#undef F\n#undef S\n")
	  (insert (format "#define F(x) %s_%s_ ## x\n"  prefix name))
	  (insert (format "#define S(x) %s_%s_ ## x\n"
			  (upcase prefix)
			  (upcase name)))
	  (insert (format "#ifdef __cplusplus\nextern \"C\" {\n#endif\n"))
	  (newline 2)
	  (insert (format "#ifdef __cplusplus\n}\n#endif\n"))
	  (newline 3)
	  (insert "#undef F\n#undef S\n")
	  (insert (format "#endif"))
	  (newline 2)
	  (insert (format "#include <%s_%s.h>\n"   prefix name))    
	  (insert (format "#define F(x) %s_%s_ ## x\n"  prefix name))
	  (insert (format "#define S(x) %s_%s_ ## x\n"
			  (upcase prefix)
			  (upcase name)))
	  (newline 2)
	  (insert "#undef F\n#undef S\n")
	  )
      (progn
	  (insert (format "#include <%s.h>\n"   name))    	
	  (insert (format "#endif"))
	  )
      )
    )

(defun bashInit (path)
  (goto-char (line-beginning-position))
  (insert "#!/bin/sh\n")
  (insert (format "script_relative_dir=%s\n" path))
  (insert "source $(dirname $0)/${script_relative_dir}function.sh\n")
  (insert "get_env ${script_relative_dir}\n")
  )

(defun genStruct (name &optional arg)
  ;; naming convention should be DataBox db;
  ;; function should be data_box_xxx()
  (let* ((s1 (if (eq arg nil)
		 (format "%s_t" name)
	       (format "%s(%s_t)" arg name)))
	 (s2 (if (eq arg nil)
		 (format "%s" name)
	       (format "%s(%s)" arg name)))
	 )
    (newline)
    (insert (format "typedef struct %s" s1))
    (newline)
    (insert (format "{"))
    (newline)
    (newline)
    (newline)
    (insert (format "} %s;" s2))
    )
  )

;; (genStruct "DataBox")

(defun printStruct (name &optional arg)
  (let* ((prec (if (eq arg nil) "" arg))
	 (str "")
	 (str1 "")
	 (output "  printf (\"")
	 (fmt  "")
	 (tail  "")
	 (pair  nil)
	 (num1 nil)
	 (num2 nil)
	 (fmt_str "")
	 (var_str "")
	 )
    (progn
      (forward-line 1)
      (forward-line 1)

      (while
	  (progn
	    (forward-line 1)
	    (not (looking-at "}")))
	(progn
	  (setq str (line-str))
	  (setq num1 (string-match ";" str))
	  (if (eq num1 nil)
	      nil
	    (progn
	      (setq pair (split-string 
			  (format "%s" (string-trim
					(substring str
						   0
						   num1)))))
	      (setq fmt_str (car pair))
	      (setq var_str (cadr pair))
	      (setq fmt (concat fmt (cond ((string= fmt_str "int")
					   (format "%s: %%d, "
						   var_str))
					  ((string= fmt_str "double")
					   (format "%s: %%%sf, "
						   var_str
						   prec))
					  ((string= fmt_str "char")
					   (if (string= (substring var_str
								   0
								   1)
							"*")
					       (progn
						 (setq var_str
						       (substring var_str
								  1))
						 (format "%s: %%s, "
							 var_str))
					     (format "%s: %%c, "
						     var_str)
					     ))
					  (t  (format "%s: %%s, "
						      var_str))
					  )))

	      (setq tail (concat tail (format ",\n  %s%s" name var_str)))
	      )))) ;; end of while
	  (forward-line 1)
	  (forward-line 1)
	  (setq fmt (substring fmt 0 -2))
	  (setq output (concat output
			       fmt
			       (format "\\n\"")
			       tail
			       (format ");\n")
			       ))
	  (insert output)
	)
      )
    )


(defun genOO (name &optional prefix)
  ;; naming convention should be DataBox db;
  ;; function should be data_box_xxx()
  "(genOO \"method\" \"class\")"
  (let* ((varname name)
	 (ooname (if (eq prefix nil)
		     name
		   (format "%s_%s" prefix name)))
	 (save (point))
	 )
    (replaceDefIfExist (format "h3oo_def (%s," ooname) "\*/")
    (next-line)
    (insert (format "\th3oo_def (%s,\n\t\th3_i\tlen;\n \
\t\th3_d\tval;\n\t\t);\n"
		    ooname))
    (insert (format "\n\th3oo_func_init (h3_void, %s);\n"
		    ooname))
    (insert (format "\n\th3oo_func_destroy (h3_void, %s);\n"
		    ooname))
    (insert (format "\n\th3oo_func (h3_void, %s, update);\n"
		    ooname))
    (insert (format "\n\th3oo_func (h3_void, %s, prepare);\n"
		    ooname))

    (insert "\n\n\n/*\n")

    (insert (format "h3oo_func_init (h3_void, %s)\n{\n\n}\n\n"
		    ooname))

    (insert (format "h3oo_func_destroy (h3_void, %s)\n{\n\n}\n\n"
		    ooname))

    (insert (format "h3oo_func (h3_void, %s, update)\n{\n\n}\n\n"
		    ooname))

    (insert (format "h3oo_func (h3_void, %s, prepare)\n{\n\n}\n\n"
		    ooname))

    (insert (format "\th3oo_init (%s, %s);\n\n"
		    varname ooname))

    (insert (format "\th3oo_call (%s, %s, update);\n\n"
		    varname ooname))

    (insert (format "\th3oo_call (%s, %s, prepare);\n\n"
		    varname ooname))

    (insert (format "\th3oo_destroy (%s, %s);\n\n"
		    varname ooname))

    (insert "*/\n")
    (goto-char save)
    )
  )


(defun genClass (name &optional arg)
  ;; naming convention should be DataBox db;
  ;; function should be data_box_xxx()
  (let* ((s1 (if (eq arg nil)
		 (format "%s_t" name)
	       (format "%s(%s_t)" arg name)))
	 (s2 (if (eq arg nil)
		 (format "%s" name)
	       (format "%s(%s)" arg name)))
	 (s3  (downcase name))
	 )
    (newline)
    (insert (format "typedef struct %s" s1))
    (newline)
    (insert (format "{"))
    (newline)
    (insert (format "%2sdouble val;" ""))
    (newline 3)
    
    (insert (format "%2svoid (*update) (struct %s \*p, double new_val);\n"
		    "" s1))
    (insert (format "%2svoid (*prepare) (struct %s \*p);\n"
		    "" s1))
    (insert (format "%2svoid (*destroy) (struct %s \*p);\n"
		    "" s1))

    (newline 2)
    (insert (format "} %s;" s2))
    (newline 3)
    (insert (format "void\nF(%s_init) (%s \*p, int para1);\n" name s2))
;    (insert (format "void\nF(%s_update) (%s \*p, double new_val);\n" name s2))
;    (insert (format "void\nF(%s_prepare) (%s \*p);\n" name s2))
;    (insert (format "void\nF(%s_destroy) (%s \*p);\n" name s2))
    (newline 5)

    (insert (format "void\nF(%s_update) (%s \*p, double new_val)\n{\n}\n\n" name s2))
    (insert (format "void\nF(%s_prepare) (%s \*p)\n{\n}\n\n" name s2))
    (insert (format "void\nF(%s_destroy) (%s \*p)\n{\n}\n\n" name s2))
    (insert (format "void\nF(%s_init) (%s \*p, int para1)\n{\n%s\n%s\n%s\n\n}\n\n"
		    name s2
		    (format "%2sp->update = F(%s_update);" "" name)
		    (format "%2sp->prepare = F(%s_prepare);" "" name)
		    (format "%2sp->destroy = F(%s_destroy);" "" name)
		    ))
    (newline 3)
    (insert (format "//%2s%s %s = {0};\n" "" s2 s3))
    (insert (format "//%2sF(%s_init) (&%s, 0);\n" "" name s3))
    (insert (format "//%2sMETHOD (%s, update, 10.0);\n" "" s3))
    )
  )

;; (genClass "EMA" "S")

  
(defun genCppClass (name)
 (let* ((kk 0)
	 )
    (newline)
    (insert (format "class %s\n{\n public:\n  %s ();\n  ~%s ();\n" name name name))
  (newline 1)
  (insert (format "%2svoid\n%4sinit ();\n" "" "")) 
  (insert (format "%2svoid\n%4sdestroy ();\n" "" "")) 
  (insert (format "%2svoid\n%4sreset ();\n" "" "")) 
    (newline)
    (insert (format "};\n"))
  (newline 5)
  (insert (format "%s::%s()\n{\n%2sinit();\n}\n" name name ""))
  (insert (format "%s::~%s()\n{\n%2sdestroy();\n}\n" name name ""))
  (insert (format "void\n%s::init()\n{\n}\n" name))
  (insert (format "void\n%s::destroy()\n{\n}\n" name))
  (insert (format "void\n%s::reset()\n{\n%2sdestroy();\n%2sinit();\n}\n" name "" ""))
    )
  )



(defun genClassFunc (rtn func arg class)
  (newline)
  (insert (format "%2s %s %s(%s);\n" "" rtn func arg))
  (insert (format "%s\n%s::%s(%s)\n{\n}\n" rtn class func arg)) 
  )


;;(printStruct "data_box")
;;(printStruct "data_box" ".2")




(defun for (idxlst start end  prefix_a prefix_b varlst &optional arg1)
  "(for '(i j k) 0 '(3 2 4) '(price vol) \"db_ptr->\" \"mediate.\" t)"
  (let* ((for_prefix (if (eq arg1 nil)
			 ""
		       "_"))
	 (idxlen (length idxlst))
	 (startlst (if (numberp start)
		       (make-list idxlen start)
		     start))
	 (endlst (if (numberp end)
		       (make-list idxlen end)
		     end))
	 (idx    0)	 
	 (prefix1 (if (eq prefix_a nil)
		      ""
		    prefix_a))
	 (prefix2 (if (eq prefix_b nil)
		      ""
		    prefix_b))
	 (varlen (length varlst))
	 (varidx    0)
	 (idxidx    0)
	 (output    "")
	 )

    (progn
      (assert (and (eq idxlen (length idxlst))
		   (eq idxlen (length idxlst))))
      (forward-line 2)

      (while 
	  (< idx idxlen)
	(progn
	  (insert (format "  FOR%s (%s, %s, %s) \n { \n"
			  for_prefix
			  (nth idx idxlst)
			  (nth idx startlst)
			  (nth idx endlst)))
	  (setq idx (+ idx 1))
	  ))
      
      ;; inner
      (while 
	  (< varidx varlen)
	(progn
	  (setq output (format "  %s%s"
			       prefix1
			       (nth varidx varlst)
			       ))
	  (setq idxidx 0)
	  (while 
	      (< idxidx idxlen)
	    (progn
	      (setq output
		    (concat output
			    (format "[%s]"
				    (nth idxidx idxlst))))
	      (setq idxidx (+ 1 idxidx))
	      )
	    )
	  (setq output
		(concat output
			(format " = %s%s"
				prefix2
				(nth varidx varlst))))
	  (setq idxidx 0)
	  (while 
	      (< idxidx idxlen)
	    (progn
	      (setq output
		    (concat output
			    (format "[%s]"
				    (nth idxidx idxlst))))
	      (setq idxidx (+ 1 idxidx))
	      )
	    )
	  (setq output
		(concat output
			(format ";\n")))
	  (setq varidx (+ varidx 1))
	  (insert output)
	  ))
      ;; tail outer
      (setq idx 0)
      (while 
	  (< idx idxlen)
	(progn
	  (insert  "  }\n")
	  (setq idx (+ idx 1))
	  ))
      )
    )
  )


;; (for '(i j k) 0 '(3 2 4)  "db_ptr->" "mediate." '(price vol) t)

;; This buffer is for text that is not saved, and for Lisp evaluation.
;; To create a file, visit it with <open> and enter text in its buffer.



(defun replaceInString (what with in)
  (if (or (eq what nil)
	  (eq with nil))
      in
    (if (eq in nil)
	nil
      (replace-regexp-in-string (regexp-quote what) with in nil 'literal)
      )
    )
  )

(defun myEval (string)
  "Evaluate elisp code stored in a string."
  (eval (car (read-from-string string))))

;;(replaceInString "*" "oeut" "*-*")
;;(replaceInString "k" "ko" "k-k")
;;(replaceInString nil nil "k-k")
;;(replaceInString "k" "ko" nil)


;;(replaceInString "" "ko" nil)

(defun genRegexp (fmt &optional mode)
  "convent fomat to specific mode"
  (progn
    (setq res (replaceInString
	       "["
	       "\\[" fmt)
	  )
    (setq res (replaceInString
	       "]"
	       "\\]" res)
	  )
    (cond ((string= mode "number")
	   (setq res (replaceInString
		      "*"
		      "\\([0-9]+\\)" res)
		 )
	   res
	   )
	  (t
	   (setq res (replaceInString
		      "*"
		      ".*" res)
		 )
	   res
	   )
	  )
    )
  )

(defun eval-format (x ptn rep &optional mode exe)
  "ptn: [*] rep: (*) mode: number exe: (1+ (sqrt *)): [55] -> (8.416)"
  (let* ((eval_str  (concat
		     "(setq num "
		     (if (eq exe nil)
			 "num"
		       (replaceInString
			"*" "num" exe)
		       )
		       ")"))
	 (ptn_str ptn)
	 (rep_str rep)
	 (x_str x)
	 (x_res "")
	 )
    (print eval_str)
    (cond ((string= mode "number")
	   (while
	       (progn
		 (setq ptn_para nil)
		 (setq x_para nil)
		 (setq rep_para nil)
		 (when
		     (string-match "*" ptn_str)
		   (setq ptn_para
			 (list
			  (match-string 0 ptn_str)
			  (match-beginning 0)
			  (match-end 0)))
		   )
		 (if ptn_para
		     (progn
		       (when
			   (string-match "*" rep_str)
			 (setq rep_para
			       (list
				(match-string 0 rep_str)
				(match-beginning 0)
				(match-end 0)))
			 )
		       (if (eq rep_para nil)
			   (progn
			     (setq x_res (concat x_res rep_str))
			     nil)
			 (progn
			   (setq head_piece (substring rep_str
						       0
						       (cadr rep_para)))
			   

			   
			   (setq x_res (concat x_res head_piece))
			   (setq x_str (substring x_str
						  (cadr ptn_para)
						  nil))

			   (when
			       (string-match "\\([0-9]+\\)" x_str)
			     (setq x_para
				   (list
				    (match-string 0 x_str)
				    (match-beginning 0)
				    (match-end 0)))
			     )
			   (setq num
				 (string-to-number
				  (car x_para)))
			   (myEval eval_str)
			   (setq x_res (concat x_res
					       (number-to-string num)))
			   (setq x_str (substring x_str
						  (caddr x_para)
						  nil))
			   (setq ptn_str (substring ptn_str
						    (caddr ptn_para)
						    nil))
			   (setq rep_str (substring rep_str
						    (caddr rep_para)
						    nil))
			   t
			   )
			 )
		       )
		   (progn
		     (setq x_res (concat x_res rep_str))
		     nil)
		   ))))
	  
	  (t
	   (progn
	     (setq rep_para nil)
	     (when
		 (string-match "*" rep_str)
	       (setq rep_para
		     (list
		      (match-string 0 rep_str)
		      (match-beginning 0)
		      (match-end 0)))
	       )
	     (setq ptn_para nil)
	     (when
		 (string-match "*" ptn_str)
	       (setq ptn_para
		     (list
		      (match-string 0 ptn_str)
		      (match-beginning 0)
		      (match-end 0)))
	       )
	     (if (or
		  (eq ptn_para nil)
		  (eq rep_para nil)
		  )
		 (setq x_res rep_str)
	       (progn
		 (setq star_str
		       (substring
			x_str
			(cadr ptn_para)
			(- (caddr ptn_para) (length ptn_str)))
		       )
		 (setq x_res
		       (replaceInString
			"*"
			star_str
			rep_str))
		 )
	       )
	     ))
	  );;cond fin				
    x_res
    )
  )


;;(insert (replaceInString "*" "oeu" "*-*"))
;;(eval-format "[45]" "[*]" "<*>" "number" "(1+ *)")
;;(eval-format "[45233_t]" "[*]" "_<*88>#oeu")
;;(eval-format "[45].[90]" "[*].[*]" "2309(*)_23.<*>23" "number" "(sqrt (1+ *))")

;; (eval-format "[OEU]" "[*]" "<_*._\"*\"qq>")

;; (eval-format "TRADE_NONE," "*," "case *: *")

(defun myReplace (ptn rep &optional line_beg line_end mode exe)
  "replace string"
  (let* (
	 (cur_point (point))
	 (line_count (count-lines (point-min) (point-max)))
	 (line_cur_idx (line-number-at-pos))
	 (reg_beg (1+ (point-at-eol)))
	 (reg_end (point-max))
	 (reg_beg_ptn "")
	 (reg_end_ptn "")
	 (fmt (genRegexp ptn mode))
	 (buffer-str "")
	 )
    ;; setup para
    (if (not (eq line_beg nil))
	(if (string= line_beg "*BEGIN*")
	    (setq reg_beg (point-min))
	  (setq reg_beg_ptn line_beg)
	  )
      )

    ;; reg_end_ptn to reset reg_end    
    (if (and (not (eq line_end nil))
	     (not (string= line_end "")))
	(progn
	  (setq reg_end_ptn line_end)
	  (goto-char reg_beg) 
	  (search-forward reg_end_ptn)
	  (setq reg_end (point))
	  )
      )
    ;; reg_beg_ptn to reset reg_beg
    (if (not (string= reg_beg_ptn ""))
	(progn
	  (goto-char reg_beg)
	  (search-forward reg_beg_ptn)
	  (backward-word)
	  (setq reg_beg (point))
	  )
      )
    
    (setq line_beg (line-number-at-pos reg_beg))
    (setq line_end (line-number-at-pos reg_end))
    ;; 
    (setq line_no line_beg)
    (while
	(progn
	  (goto-line line_no)
	  (<= line_no line_end)
	  )
      (if (eq line_no line_cur_idx)
	  (progn
	    (setq line_no (1+ line_no))
	    (setq buffer-str (concat buffer-str
				     (format "%s\n"
					     (line-str))))
	    t
	    )
	(setq line_str (line-str))
	(setq line_res "")
	;; find-str

	(cond
	 ((string= mode "*")
	  (setq line_para nil)
	  (when
	      (string-match fmt line_str)
	    (setq line_para
		  (list
		   (match-string 0 line_str)
		   (match-beginning 0)
		   (match-end 0)))
	    )
	  (setq line_piece (substring line_str
				      0
				      (caddr line_para)))
	  (if (eq line_para nil)
	      (setq rep_res (car line_para))
	    (setq rep_res
		  (eval-format
		   (car line_para)
		   ptn rep mode exe))
	    )
	  (setq line_res rep_res)
	  )
	 ((string= ptn "*")
	  (setq line_res
	    (replaceInString
	     "*"
	     line_str
	     rep
	     )
	    )
	  )
	 ((or (string= mode "number") t)
	  (while
		   (progn
		     (setq line_para nil)
		     (when
			 (string-match fmt line_str)
		       (setq line_para
			     (list
			      (match-string 0 line_str)
			      (match-beginning 0)
			      (match-end 0)))
		       )
		     (setq line_piece (substring line_str
						 0
						 (caddr line_para)))
		     (if (eq line_para nil)
			 (setq rep_res (car line_para))
		       (setq rep_res
			     (eval-format
			      (car line_para)
			      ptn rep mode exe))
		       )
		     (setq line_res (concat line_res
					    (replaceInString
					     (car line_para)
					     rep_res
					     line_piece)))
		     line_para
		     )
		 (setq line_str (substring
				 line_str
				 (caddr line_para)
				 nil))
		 ))
	      )
	
	(setq buffer-str (concat buffer-str
				 (format "%s\n" line_res)))
	(setq line_no (1+ line_no))	
	)
      )
;;    (print buffer-str)

    (goto-line line_beg)
    (kill-line (1+ (- line_end line_beg)))
    (insert buffer-str)
    (goto-char cur_point)
    (format "%s (%d [%d %d]) begptn: %s endptn: %s line no (%d/%d [%d %d])"
	    mode cur_point reg_beg reg_end
	    reg_beg_ptn reg_end_ptn
	    line_cur_idx line_count
	    line_beg line_end
	    )
    )
  )

