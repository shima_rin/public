* Install and uninstall
** install
   - install the offline- english version tws gateway
   - follow the download page to install
   - during install, remeber the version 963
** uninstall
   - goto ~/Jts
   - sh uninstall


* IBController
** setup
   - copy the ini file into the ~/Documents
   - change XXXStart.sh && XXXGateWayStart.sh 's version to 963
   - also needs to install the xterm
   - fake X server

      #+BEGIN_SRC bash
# let us add simplistic graphical user interface support on remote ubuntu:

# Install xvfb - Virtual Framebuffer 'fake' X server

sudo apt install -y xvfb


# for now just run virtual frame buffer manually:

/usr/bin/Xvfb :0 -ac -screen 0 1024x768x24 &

      #+END_SRC

** headless
*** .ini
       #+BEGIN_SRC bash    
    IbControllerPort=7462
    IbAutoClosedown=no
    AcceptNonBrokerageAccountWarning=yes
    ReadOnlyLogin=no
    IbLoginId=illya520
      #+END_SRC

*** .sh
       #+BEGIN_SRC bash    
   TWS_MAJOR_VRSN=963
   IBC_INI=/root/Code/tws/IBController.ini
   TRADING_MODE=
   IBC_PATH=/root/Code/tws/IBController-3.4.0/
   TWS_PATH=~/Jts
   LOG_PATH=~/IBController/Logs
      #+END_SRC

*** start , check kill
    you have to enable in desktop environment the activeX and disable read-only
    
   #+BEGIN_SRC bash
   vncserver :2; //vncserver -kill :2
   sudo DISPLAY=:2 ./IBControllerStart.sh 
   cat ../Logs/*GateWay*.ini
   pkill IB[tab]
   #+END_SRC

* IB api
** sample
  - go to http sample
  - make to install
  - ./xxx localhost $(port) in the last session

** check 
  - you will see 'IBControllerServer accepted connection from: 127.0.0.1' in the log file
    
** data
  - checking ContractSample.cpp to see how to identify a contract
  - tickPrice to indicate the latest ask/bid/last price, and one time last/how/low infomation
  - tickSize to indicate the volume information

    
