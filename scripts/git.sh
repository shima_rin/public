#!/bin/sh
script_relative_dir=./
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}

if [ $if_git_dir = 0 ];then
    exit
fi

branch=`git rev-parse --abbrev-ref HEAD`
url=$(sga -m1 -M 1 -b "url" -n 3 $git_root_dir/.git/config)
repo_owner=$(echo $url | sga -m2 -b : -e /)
repo_name=$(echo $url | sga -m3 -e /)
repo_name_clean=$(echo $repo_name | sga -m4 -b "\.git")

tmp3=$(echo "$url" | head -c3)
if [ "$tmp3" = "git" ]; then
    tmp_prefix="git@"
    domain=$(echo $url | sga -m2 -b git@ -e :)
    http_url=$(echo $url | sga -m101 -c "git@$domain:" -r "https://$domain/")
    git_url=$url
    clone_method=ssh
else
    git_url=$url
    http_url=$url
    clone_method=http
fi

provider=$(echo $domain | sga -m4 -b _)
developer=$(echo $domain | sga -m101 -c "^${provider}_" -r "")

fork_url=$(git remote -v | sga -m0 -c "upstream" \
		   | sga -m0 -c "(fetch)" \
		   | sga -m2 -b "upstream " -e " (fetch)")

fork_main_branch=$(git branch -r | sga -m0 -c "upstream" \
			   | sga -m3 -e "/")

case $1 in
    restore)
	read -p "undo add file (can be multiple): " undo_file		
	git checkout -- $undo_file
	;;
    a)
	git add .
	;;
    a-undo)
	read -p "undo add file: [(all)]: " undo_file	
	git reset $undo_file
	;;
    c)
	read -p "commit msg: [automatic push]: " msg
	msg=${msg:-"automatic push"}
	git add .
	git diff-index --quiet HEAD || git commit -a -m "$msg"
	;;
    c-undo)
	read -p "undo commits count: [1 (one undo)]: " undo_count	
	undo_count=${undo_count:-1}
	git reset HEAD~$undo_count
	;;
    c-merge)
	git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all
	read -p "merge commits count: [1 (no merge)]: " merge_count	
	merge_count=${merge_count:-1}
	git reset --soft HEAD~$merge_count
	tmp=`git log --format=%B --reverse HEAD..HEAD@{1}`
	read -p "commit comment: [$tmp]: " tmp_commit
	tmp_commit=${tmp_commit:-$tmp}
	git commit -a -m "$tmp_commit"
	;;
    push)
	read -p "push remote on: [origin]: " remote
	remote=${remote:-origin}
	read -p "commit msg: [automatic push]: " msg
	msg=${msg:-"automatic push"}
	git add .
	git diff-index --quiet HEAD || git commit -a -m "$msg"
	git push -u $remote $branch
	;;
    pull)
	git fetch
	git checkout
	git pull
	;;
    refresh)
	git rm -r --cached .
	git add .
	git commit -m "refresh cache"
	;;
    d)
	git icdiff
	;;
    log) 
	git log
	;;
    graph)
	git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''          %C(white)%s%C(reset) %C(dim white)- %an%C(reset)' --all
	echo ""
	;;
    init)
	echo "must goto root dir first..., if not rm -rf .git"
	git init
	git checkout -b main && git add .
	git commit -m "init"
	git remote add origin $2
	git push -u --force origin main
	;;
    clear-commit-history)
	echo "goto setting/repo/proctected/ allow maintainer to force push"
	cp -rf $git_root_dir/.git $git_root_dir/.git_backup
	rm -rf $git_root_dir/.git
	cd $git_root_dir && git init
	git checkout -b main && git add .
	git commit -m "clear commit history"
	git remote add origin $url
	git push -u --force origin main
	;;
    switch-to-git)
	git remote set-url origin $git_url
	;;
    switch-to-http)
	git remote set-url origin $http_url
	;;
    info)
	git remote show origin
	echo domain is $domain, branch is $branch
	echo git_url is $git_url, http_url is $http_url
	;;
    b)
	git checkout $2
	;;
    b-a)
	git checkout -b $2
	;;
    b-d)
	git branch -D $2
	;;
    b-d-remote)
	read -p "delete remote on: [origin] " remote
	remote=${remote:-origin}
	read -p "delete branch: " branch_being_deleted
	git push $remote --delete $branch_being_deleted
	;;
    b-info)
	git branch -a
	echo "----------------------"
	git branch -r
	;;
    b-rename)
	read -p "rename original name: [$branch] " del_branch
	del_branch=${del_branch:-$branch}
	read -p "to name: " new_name
	if [ -z "$new_name" ]; then
	    exit
	fi
	git checkout -m $del_branch $new_name
	git push origin :$del_branch
	git push origin $new_name
	;;
    m)
	read -p "merge branch being absorbed: " branch_being_absorbed
	read -p "merge into branch absorb others: " branch_absorb_others
	git checkout $branch_being_absorbed
	git add .
	git commit -a -m "last commit before being absorbed"
	git checkout $branch_absorb_others
	git merge $branch_being_absorbed
	read -p "if delete the tmp branch: [n]" yn
	yn=${yn:-n}
	if [ "$yn" = "y" ]; then
	    git branch -D $branch_being_absorbed
	fi
	;;
    m-remote)
	read -p "merge remote on: [origin] " remote
	remote=${remote:-origin}
	read -p "merge remote branch being absorbed: [wip] " branch_being_absorbed
	branch_being_absorbed=${branch_being_absorbed:-wip}
	read -p "merge into branch absorb others: [$branch] " branch_absorb_others
	branch_absorb_others=${branch_absorb_others:-$branch}
	git fetch $remote $branch_being_absorbed
	git checkout $branch_absorb_others
	git merge ${remote}/$branch_being_absorbed
	;;
    fork)
	echo "git to gitlab: create project/repo by url/set same project name"
	echo "use p clone to clone to local then cd to the maintainer's directory"
	read -p "original repo by url: " upstream_url
	git remote add upstream $upstream_url
	git remote -v
	fork_main_branch=`git branch -r | sed -n '/upstream/{//p;q}' | sed -e 's+upstream/++g'`
	if [ "$fork_main_branch" != "main" ]; then
	    git branch -m $fork_main_branch main
	    git push origin main
	    git checkout main
	fi
	;;
    fork-update)
	git fetch upstream
	git checkout main
	git merge upstream/$fork_main_branch
	;;
    sub-a)
	read -p "add submodule by url: " sub_url
	read -p "add submodule to dir: " sub_dir
	git submodule add $sub_url $sub_dir
	cd $git_root_dir/$sub_dir && git checkout -b sub/
	git commit -m "add submodule $sub_url to $sub_dir"
	git submodule init
	git submodule update
	;;
    sub-init)
	git submodule init
	git submodule update
	;;
    sub-pull)
	git submodule for each 'git pull'
	;;
    sub-d)
	read -p "delete submodule by name: " sub_name	
	git submodule deinit $sub_name
	git rm $sub_name
	;;
    sub-h)
	echo "main project only track submodule commit version"
	echo "--if main project is pulled, submodule dir is empty"
	echo "----so update is to update submodule dir to the recorded version"
	echo "--if sub dir is dirty but not committed, main project is not changed"
	echo "----so go to sub dir and commit"
	echo "--if remote is changed, as main project's commit version is not changed"
	echo "----so go to sub dir and pull"
	;;
    r)
	git remote -v
	;;
    r-a)
	read -p "remote user: " remote_user
	read -p "remote hostname, will be deleted if exists: " remote_domain
	git remote rm $remote_domain
	remote_name=$remote_domain
	echo "if the wanted dir is /home/$remote_user/Code/$repo_name_clean/$developer, you only need to tell the path 'Code', then it will be created"
	read -p "remote git path: " remote_path
	remote_full_path="/home/$remote_user/$remote_path/$repo_name_clean/$developer"
	ssh vm "mkdir -p $remote_full_path && cd $remote_full_path && git init && git config receive.denyCurrentBranch ignore"	
	echo "git remote add $remote_name $remote_user@$remote_domain:$remote_full_path"
	git remote add $remote_name $remote_user@$remote_domain:$remote_full_path
	git push $remote_domain
	ssh $remote_domain "cd $remote_full_path && git reset --hard && git checkout $branch"	
	;;
    r-d)
	read -p "delete remote url: " del_name
	git remote rm $del_name
	;;
    r-push)
	read -p "push remote on [vm]: " remote
	remote=${remote:-vm}
	remote_path=$(git remote -v | sga -m0 -c vm \
			      | sga -m0 -M1 -c "(push)" \
			      | sga -m2 -b "$remote:" -e " (push)")
	read -p "commit msg: [automatic push]: " msg
	msg=${msg:-"automatic push"}
	git add .
	git diff-index --quiet HEAD || git commit -a -m "$msg"
	git push $remote
	echo "ssh $remote 'cd $remote_path && git reset --hard'"
	ssh $remote "cd $remote_path && git reset --hard"
	;;
    commands)
	if [ -z "$2" ]; then
	echo "Usage: $0 <arg1,[arg2]...> #comment"
	printf "%80s\n" |tr " " "-"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "a #add" "c <[msg]> #commit" "push #current branch" "pull"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "log" "diff" "info" "graph"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "init <remote url>" "refresh #cache" "clear-commit-history" "h <[word]> #help"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "fork #init only" "fork-update #merge" "switch-to-git" "switch-to-http"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "b <[branch> #switch" "b-a <branch> #add" "b-d <branch> #local" "b-d-remote #delete remote"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "m  #merge local" "m-remote  #merge" "b-info" "c-merge #merge or modify commit"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "restore  #to last" "a-undo " "c-undo " "sub-h #submodule instruction"
	printf "\t%-20s| %-20s| %-20s| %-20s\n" "sub-a  #add remote" "sub-init  #empty dir" "sub-d #delete " "sub-pull #pull remote " 
	else
	    git help $2
	fi
	;;
    h)
	printf "%80s\n" |tr " " "-"
	printf "1. %s\n" "whole picture"
	printf "\t%s\n" "two git users, one project holder(illya/rin), one worker(miyu/kato)"
	printf "\t%s\n" "holder creates the project, teamed worker, create 'main' 'wip'"
	printf "\t%s\n" "worker work in 'wip', create 'tmp' branch [b-a tmp]"
	printf "\t\t%s\n" "when done, merge into local 'wip' [b m\ntmp\nwip\ny\n], then [push]"
	printf "\t%s\n" "holder merge remote 'wip' into 'main' [b m-remote\ny\nwip\nmain\n], then [push]"
	
	printf "2. %s\n" "work in 'tmp'"
	printf "\t%s\n" "[a] add all files for commit, then [a-undo] those not wanted: untracked files"
	printf "\t%s\n" "[c] commit all added files, then [restore] bad changes to commited time"
	printf "\t\t%s\n" "add is whether you want the file included in the project"
	printf "\t\t%s\n" "commit is whether you want to save the change so far"
	printf "\t%s\n" "[c-merge] merge many commits into one, [c-undo] undo commits"
	printf "\t\t%s\n" "'f1' commit1 'f2' commit2 'f3' [c-merge] -> commit12 'f3'"
	printf "\t\t%s\n" "'f1' commit1 'f2' commit2 'f3' [c-undo] -> commit1 'f2' 'f3'"
	printf "\t%s\n" "after undo, 'git fetch --all --prune', then solve the conflict and push"
	printf "\t%s\n" "[clear-commit-history] clear all commits like new project"
	
	printf "3. %s\n" "submodule"
	printf "\t%s\n" "[sub-a] to add new submodule, [sub-init] to init an already added but empty sub"
	printf "\t%s\n" "[sub-pull] pull submodule to the new, [sub-d] to delete submodule"
	printf "\t%s\n" "main project only track submodule commit version"
	printf "\t\t%s\n" "[sub-pull] only pull to that version"
	printf "\t\t%s\n" "if sub is dirty but not commited, main is not changed"
	printf "\t\t%s\n" "if sub is dirty and commited, version changed so main is changed"
	printf "\t\t\t%s\n" "so if you pull, there is a confliction"
	printf "\t%s\n" "if you get into the submodule dir, modify and push, remember to push the main"
	
	printf "4. %s\n" "remote machine"
	printf "\t%s\n" "[r-a] to add new sync dir on remote machine."
	printf "\t%s\n" "[r-push] push the change to the machine, [r-d] to delete remote machine"
	printf "\t%s\n" "[r] to check all the remote, all remote will be hard reset set as the push and no origin"

	printf "5. %s\n" "others"
	printf "\t%s\n" "[log] [graph] [diff]"
	printf "\t%s\n" "script project: git@gitlab_shima_rin:shima_rin/public.git"
	printf "\t%s\n" "test project: git@gitlab_shima_rin:shima_rin/git_test.git"
	;;
    tmp)
	
	;;
    *)
	git status
	printf "%80s\n" |tr " " "-"
	echo  "repo: $repo_name, provider: $provider, domain: $domain, method: $clone_method"
	echo  "developer: $developer, owner: $repo_owner"

	if [ -z "$fork_url" ]; then
	    echo "cannot find fork upstream, type 'g h' for help, 'g manual' for more"
	else
	    echo "git fork from $fork_url"
	    echo "git fork main branch is $fork_main_branch"
	fi
	printf "%80s\n" |tr " " "-"
	;;
esac

