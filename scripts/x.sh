#!/bin/bash
script_relative_dir=./
. $(dirname $0)/${script_relative_dir}function.sh
get_env ${script_relative_dir}

case $1 in
    hosts)
	cat /etc/hosts
	;;
    hosts-add)
	read -p "host name: " host_name 		
	read -p "host ip: " host_ip 		
        echo "sudo chmod 666 /etc/hosts && echo '$host_ip $host_name' >> /etc/hosts && sudo chmod 644 /etc/hosts"
	echo "already copy into pasteboard"
	if [ "$os" = "Darwin" ];then
	    echo "sudo chmod 666 /etc/hosts && echo '$host_ip $host_name' >> /etc/hosts && sudo chmod 644 /etc/hosts" | pbcopy
	else
	    xclip "sudo chmod 666 /etc/hosts && echo '$host_ip $host_name' >> /etc/hosts && sudo chmod 644 /etc/hosts" 
	fi
	;;
    sshconfig)
	cat ~/.ssh/config
	;;
    saychn)
	say -v Ting-Ting $2
	;;
    sayjpn)
	say -v kyoko $2
	;;
    sayfchn)
	say -v Ting-Ting -f $2
	;;
    sayfjpn)
	say -v kyoko  -f $2
	;;
    say)
	say -v $2 $3
	;;
    sayall)
	for i in `say -v '?' | cut -d ' ' -f 1`; do echo $i && say -v "$i" $2;done
	;;
    say-save)
	say $2 -o tmp.aiff && lame -m m tmp.aiff $3.mp3 && rm tmp.aiff
	;;
    sayf)
	say -v $2 -f $3;
	;;
    sayf-save)
	say -f $2 -o tmp.aiff && lame -m m tmp.aiff $3.mp3 && rm tmp.aiff
	;;
    hide)
	stegify encode -carrier $3 -data $2 -result $3_xx.png #if string of multiply, sep by space
	;;
    show)
	stegify decode -c $2 -r $3
	;;
    album)
	osascript $script_dir/mac/export_album.scpt $2 $3
 	;;
    icon)
	echo "open icon directory and put image in it and renamed icon.png"
	mkdir -p icon/icons.iconset
	rm -rf icon/icons.iconset/*
	sips -z 16 16     icon/icon.png --out icon/icons.iconset/icon_16x16.png
	sips -z 32 32     icon/icon.png --out icon/icons.iconset/icon_16x16@2x.png
	sips -z 32 32     icon/icon.png --out icon/icons.iconset/icon_32x32.png
	sips -z 64 64     icon/icon.png --out icon/icons.iconset/icon_32x32@2x.png
	sips -z 64 64     icon/icon.png --out icon/icons.iconset/icon_64x64.png
	sips -z 128 128   icon/icon.png --out icon/icons.iconset/icon_64x64@2x.png
	sips -z 128 128   icon/icon.png --out icon/icons.iconset/icon_128x128.png
	sips -z 256 256   icon/icon.png --out icon/icons.iconset/icon_128x128@2x.png
	sips -z 256 256   icon/icon.png --out icon/icons.iconset/icon_256x256.png
	iconutil -c icns icon/icons.iconset 
	;;
    install-script)
	osacompile -o ~/Applications/$3.app $2.scpt && echo "install success"	
	;;
    run)
	osascript $2.scpt
	;;
    sex)
	run_tool browse sexinsex
	;;
    hupu)
	run_tool browse hupu
	;;
    lol)
	run_tool browse lol
	;;
    kindle)
	ebook-convert $1 /run/media/stan/Kindle/documents/diy/$2.azw3 --title $2
	;;
    dl)
	rm -f ~/Downloads/sys_dl_tmp*
	rm -f ~/Downloads/convert_video/sys_dl_tmp*
	url=`pbpaste`
	str=$(you-get -f  -o ~/Downloads/convert_video/  -O sys_dl_tmp $url)
	title=$(echo $str | sga -m2 -b "title: " -e " stream:")
	fmt=$(echo $str | sga -m2 -b "container: " -e " quality:")
	file=sys_dl_tmp.$fmt
	new_file=~/Downloads/$title.mp4
	echo $title
	run_tool convert_video
	mv ~/Downloads/sys_dl_tmp.mp4 "$new_file"
	echo "wait for completion"
	pause_for_key
	quit_app "Convert Video"
	open_file "Music" "$new_file"
	echo "/Users/yuanchuan/Music/Music/Media.localized/Unknown Artist/Unknown Album/$title.mp4" >> ~/Music/fav.m3u
	sleep 3
	quit_app "Music"
	rm -f ~/Downloads/sys_dl_tmp*
	rm -f ~/Downloads/convert_video/sys_dl_tmp*
	rm -f "$new_file"
	echo "dl finished..."
	;;
    dl-update)
	pip3 install --upgrade you-get
	;;
    close-tabs)
	run_tool safari_close_tabs $2
	;;
    backup_config)
	mkdir -p backup_config
	cp ~/Music/*.m3u backup_config/
	;;
    close)
	quit_app $2
	;;
    comic)
	if_mac
	config_get comic_path
	cd $comic_path
	comic_list=()
	comic_idx=0
	for d in */ ; do
	    comic_list+=("$d")
	    echo "[$comic_idx] $d"
	    comic_idx=$((comic_idx + 1))
	done
	read -p "pick a comic book: " comic_idx
	cd "${comic_list[$comic_idx]}"
	open *
#	file=$(ls | grep ".*\.[jpg|jpeg|png]" | head -1)
#	fileext=${file##*.}
	#	open *.$fileext
	fi
	;;
    concat-movies)
	echo "https://stackoverflow.com/questions/7333232/how-to-concatenate-two-mp4-files-using-ffmpeg"
	ffmpeg -f concat -safe 0 -i $2 -c copy $3.mp4
	;;
    concat-movies-auto)
	echo "fmt=mov x concat-movies-auto to cotcat videos.mov(default mp4) in pwd" 
	rm -rf convert_list.lisp
	if [ -z "$fmt" ];then
	    for filename in $current_dir/*.mp4; do
		echo "file $filename" >> convert_list.lisp
	    done
	else
	    for filename in $current_dir/*.$fmt; do
		echo "file $filename" >> convert_list.lisp
	    done
	fi
	ffmpeg -f concat -safe 0 -i convert_list.lisp -c copy output.mp4
	rm -rf convert_list.lisp
	;;
    cut-movie)
	echo "x cut-movie src.mp4 00:30 06:30, src.mp4 copied to orig_src.mp4" 
	mv $2 original_$2
	ffmpeg -ss $3 -to $4 -i original_$2 -metadata:s handler_name=SoundHandler -c copy $2
	;;
    record-screen)
	echo "cmd+shift 5 to end"
	osascript -e 'tell application "System Events" to key code 23 using {shift down,command down}'
	;;
    qqmusic)
	cd ~/Downloads/
	./decoder
	;;
    tmp)
	echo $current_dir
	echo $file_path
	echo $file_dir
	echo $script_path
	echo $script_dir
	;;
    test)
	str=site:\ Bilibili\ title:\ 《送给所有想学刀妹的人》\ stream:\ -\ format:\ dash-flv720\ container:\ mp4
	echo $str
	echo title is: $title fmt is: $fmt
	;;
    
esac
