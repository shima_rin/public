#!/bin/bash
base_path=$(realpath "$0")
base_dir=$(dirname $base_path)

create_gitignore () {
    echo $1 >> .gitignore
}

if [ $1 ]
then
    case $1 in
	encrypt)
	    if [ $3 ]
	       then
		   echo "encrypt file $2 to $3"
		   gpg2 --output $3 --encrypt --recipient cyc@illya.co $2
	    else
		echo "encrypt file $2 to $2.encrypted"
		gpg2 --output $2.encrypted --encrypt --recipient cyc@illya.co $2
	    fi
	    ;;
	decrypt)
	    if [ $3 ]
	       then
		   echo "decrypt file $2 to $3"
		   gpg2 --output $3 --decrypt $2
	    else
		echo "decrypt file $2 to $2.decrypted"
		gpg2 --output $2.decrypted --decrypt $2
	    fi
	    ;;
	sign)
	    echo "sha file $2 to $2.sha256sum"
	    echo "sign file $2 to $2.sha256sum.sig"
	    shasum -a 256 $2 | awk '{print $1}' > $2.sha256sum
	    gpg2 --output $2.sha256sum.sig --sign $2.sha256sum
	    ;;
	verify)
	    echo "shasum -a 256 $2"
	    checksum_local=`shasum -a 256 $2 | awk '{print $1}' `
	    checksum_remote=`cat $2.sha256sum`
	    gpg2 --verify $2.sha256sum.sig
	    echo "local result is $checksum_local"
	    echo "remote result is $checksum_remote"
	    if [ "$checksum_local" =  "$checksum_remote" ]
	       then
		   echo "PASS ! please also check the signature above! "
	    else
		echo "FAIL !"
	    fi
	    ;;
	export)
	    gpg2 --armor --export cyc@illya.co
	    gpg2 --export-ssh-key cyc@illya.co
	    ;;
	*)
	    echo "supported typeOfProject: encrypt | decrypt | sign | verify | export"
	    ;;
    esac

else
    echo "wrong synatx"
    echo "synatx: yubico method filein [fileout]"
    echo "supported typeOfProject: encrypt | decrypt | sign | verify | export"
    echo "eg. newP encrypt filein fileout"
fi

